// ConsoleApplication5.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"
#include "stdio.h"


int main(void)
{
	int x = 10;//int(整数)
	float y = 5.2;//float(実数)
	char c = 'A';//char(一文字)

	printf("x=%d,y=%f,c=%c\n", x, y, c);

    return 0;
}

